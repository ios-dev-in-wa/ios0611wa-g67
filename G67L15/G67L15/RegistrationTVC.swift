//
//  RegistrationTVC.swift
//  G67L15
//
//  Created by Ivan Vasilevich on 1/8/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class RegistrationTVC: UITableViewController {
	let text = """
RegExr was created by gskinner.com, and is proudly hosted by Media Temple.

Edit the Expression & Text to see matches. Roll over matches or the expression for details. PCRE & Javascript flavors of RegEx are supported.
$300 20
The side bar includes a Cheatsheet, full Reference, and Help. You can also Save & Share with the Community, and view pa$306tterns you create or favorite in My Patterns.

Explore results with the Tools below. Replace & List output custom results. Details lists capture groups. Explain describes your expression in plain English.
"""
	@IBOutlet weak var secondTextField: UITextField!
	
    override func viewDidLoad() {
        super.viewDidLoad()

		
    }
	
	override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
		view.endEditing(true)
		let baks = """
(\\$)
"""
		let matchesForRegEx = text.matches(for: "\(baks)[0-9]{2,3}")
		secondTextField.text = matchesForRegEx.joined(separator: "|")
	}

}

extension RegistrationTVC: UITextFieldDelegate {
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		if textField === secondTextField {
			textField.resignFirstResponder()
		}
		else {
			secondTextField.becomeFirstResponder()
		}
		
		return false
	}
	
	

	
}

extension String {
	func matches(for regex: String) -> [String] {
		
		do {
			let regex = try NSRegularExpression(pattern: regex)
			let results = regex.matches(in: self,
										range: NSRange(self.startIndex..., in: self))
			return results.map {
				String(self[Range($0.range, in: self)!])
			}
		} catch let error {
			print("invalid regex: \(error.localizedDescription)")
			return []
		}
	}
}
