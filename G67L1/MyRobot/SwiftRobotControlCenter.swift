//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//

/*
,dfl;asnfdmfdf';dlf
l
f;
'flds;'flds;'flds'flds'fa
'
fmjkldsanfdf
*/

import UIKit
//all robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
	
	//in this function change levelName
	override func viewDidLoad() {
		levelName = "L4H" // level name
		
		super.viewDidLoad()
	}
	
	
	
	override func viewDidAppear(_ animated: Bool) {
		
		super.viewDidAppear(animated)
		
		let a = 5.0, b = 4.0
		
		let sqOfNumA = squareOfNumber(num: a)
		let sqOfNumB = squareOfNumber(num: b)
		let sumOfSquares = sqOfNumA + sqOfNumB
		print("sum of sq \(a) and \(b) = \(sumOfSquares)")
		
	}
	
	func smartMove(steps: Int) {
		for _ in 0..<steps {
			move()
		}
	}
	
	func smartMove(steps: Int,
				   needToPutCandy: Bool) {
		for _ in 0..<steps {
			move()
			if needToPutCandy {
				put()
			}
		}
	}
	
	func sayHello() {
		print("hello")
	}
	
	func squareOfNumber(num: Double) -> Double {
		let result = num * num
		print("square of \(num) = \(result)")
		return result
	}
	
	func turnLeft() {
		turnRight()
		turnRight()
		turnRight()
	}
	
}
