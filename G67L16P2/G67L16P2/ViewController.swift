//
//  ViewController.swift
//  G67L16P2
//
//  Created by Ivan Vasilevich on 1/11/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit
import GoogleMobileAds

class ViewController: UIViewController {
	
	@IBOutlet var bannerView: GADBannerView!

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		// In this case, we instantiate the banner with desired ad size.
//		bannerView = GADBannerView(adSize: kGADAdSizeBanner)
		bannerView.adUnitID = "ca-app-pub-8526587912608693/5399494121"
		bannerView.rootViewController = self
//		addBannerViewToView(bannerView)
		bannerView.load(GADRequest())
	}

	func addBannerViewToView(_ bannerView: GADBannerView) {
		bannerView.translatesAutoresizingMaskIntoConstraints = false
		view.addSubview(bannerView)
		view.addConstraints(
			[NSLayoutConstraint(item: bannerView,
								attribute: .bottom,
								relatedBy: .equal,
								toItem: bottomLayoutGuide,
								attribute: .top,
								multiplier: 1,
								constant: 0),
			 NSLayoutConstraint(item: bannerView,
								attribute: .centerX,
								relatedBy: .equal,
								toItem: view,
								attribute: .centerX,
								multiplier: 1,
								constant: 0)
			])
	}
	

}

