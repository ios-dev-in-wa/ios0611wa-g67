//
//  ViewController.swift
//  G67L5
//
//  Created by Ivan Vasilevich on 11/20/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		
//		optionalUnwraping()
//		arrayForHomeWork()
//		dictionary()
		drawBox()
	}
	
	func optionalUnwraping() {
		let numberString = "123"
		let optionalIntFromString = Int(numberString)
		
		// optional binding
		if let realValue = optionalIntFromString {
			print("optionalIntFromString = \(realValue)")
		}
		else {
			print("optionalIntFromString = nil")
		}
		
		// force unwrap !!!
		print(optionalIntFromString! + 5)
		
		let report = "5"
		// default value
		let countOfStudents = Int(report) ?? 0
		print("students count = \(countOfStudents)")
	}
	
	func arrayForHomeWork() {
//		let a = Int.init()
//		let s = String.init()
		// T.init() == T()
//		var arrayOfNumbers = [Int].init()
//		var arrayOfNumbers = [Int]()
//		var arrayOfNumbers: [Int] = []
		var arrayOfNumbers = Array<Int>.init()
		for _ in 0..<20 {
			arrayOfNumbers.append(Int.random(in: 0...10))
		}
		print(arrayOfNumbers)
	}
	
	func dictionary() {
		let tachki = ["hachiRoku" : "Toyota AE86",
					  "Eleonore" : "Ford GT350"]
		let resqueServicesNumbers = ["101", "102"]
		var phoneBook = ["emergency" : resqueServicesNumbers,
						 "contACT2" : ["QWERT678IOP", "0987654321"]]
		print(phoneBook["hachiRoku"] ?? ["unknown number"])
		phoneBook["newNumber"] = ["12345678", "09876543"]
	}
	
	func drawBox() {
		let box = UIView.init()
		box.frame.size.width = 100
		box.frame.size.height = 200
		box.frame.origin.x = 64
		box.frame.origin.y = 128
		box.backgroundColor = UIColor(red: CGFloat(arc4random_uniform(256))/255,
									  green: CGFloat(arc4random_uniform(256))/255,
									  blue: CGFloat(arc4random_uniform(256))/255,
									  alpha: 1)
		view.addSubview(box)
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		drawBox()
	}

}

