//
//  ListDetailViewController.swift
//  G67L-Tung
//
//  Created by Tung Fam on 12/6/18.
//  Copyright © 2018 Tung Fam. All rights reserved.
//

import UIKit

class ListDetailViewController: UIViewController {

    private let item: Item

    var listDetailView: ListDetailView {
        return view as! ListDetailView
    }

    init(item: Item) {
        self.item = item
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        listDetailView.render(item: item)
    }

}
