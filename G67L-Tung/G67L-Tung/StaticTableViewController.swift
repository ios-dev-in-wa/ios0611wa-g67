//
//  StaticTableViewController.swift
//  G67L-Tung
//
//  Created by Tung Fam on 12/6/18.
//  Copyright © 2018 Tung Fam. All rights reserved.
//

import UIKit

class StaticTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

}
