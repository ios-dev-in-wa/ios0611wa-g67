//
//  ItemsService.swift
//  G67L-Tung
//
//  Created by Tung Fam on 12/6/18.
//  Copyright © 2018 Tung Fam. All rights reserved.
//

import UIKit

class ItemsService {

    func getItems() -> [Item] {

        var array = [Item]()

        for _ in 1..<100 {

            let item = Item(
                title: "iPhone", image: #imageLiteral(resourceName: "iphone"),
                price: Double.random(in: 1..<1000),
                isPresent: Bool.random(),
                quantity: Int.random(in: 1..<50)
            )

            array.append(item)
        }

        return array
    }

}
