//
//  Item.swift
//  G67L-Tung
//
//  Created by Tung Fam on 12/6/18.
//  Copyright © 2018 Tung Fam. All rights reserved.
//

import UIKit

struct Item {
    let title: String
    let image: UIImage
    let price: Double
    let isPresent: Bool
    let quantity: Int
}

extension Item {
    var showableDescription: String {
        let rounded = Double(Int(price * 100)) / 100
        return "$\(rounded)"
    }
}
