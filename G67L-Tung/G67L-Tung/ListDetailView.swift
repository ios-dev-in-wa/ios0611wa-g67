
//
//  ListDetailView.swift
//  G67L-Tung
//
//  Created by Tung Fam on 12/6/18.
//  Copyright © 2018 Tung Fam. All rights reserved.
//

import UIKit

class ListDetailView: UIView {

    @IBOutlet weak var priceLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

        setup()
    }

    private func setup() {

    }

    func render(item: Item) {
        priceLabel.text = "\(item.showableDescription)"
    }
}
