//
//  ItemCell.swift
//  G67L-Tung
//
//  Created by Tung Fam on 12/6/18.
//  Copyright © 2018 Tung Fam. All rights reserved.
//

import UIKit

class ItemCell: UITableViewCell {

    @IBOutlet private weak var itemImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var quantityLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

    private func setup() {
        quantityLabel.textAlignment = .right

        itemImageView.contentMode = .scaleAspectFit
    }

    func render(item: Item) {
        titleLabel.text = item.title

        itemImageView.image = item.image

        quantityLabel.text = "\(item.quantity) pcs"

        priceLabel.text = item.showableDescription
    }
    
}

