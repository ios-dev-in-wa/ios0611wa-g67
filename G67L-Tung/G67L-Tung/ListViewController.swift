//
//  ListViewController.swift
//  G67L-Tung
//
//  Created by Tung Fam on 12/6/18.
//  Copyright © 2018 Tung Fam. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ListViewController: UIViewController {

    private enum Constants {
        static let itemCellIdentifier = "ItemCell"
    }
    
    @IBOutlet private weak var tableView: UITableView!

    private let itemsService = ItemsService()

    var items = [Item]()

    private var loader: NVActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()

        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.items = self.itemsService.getItems()
            self.tableView.reloadData()
            self.loader.stopAnimating()
        }

        let loaderFrame = CGRect(x: 100, y: 100, width: 50, height: 50)
        loader = NVActivityIndicatorView.init(frame: loaderFrame, type: .ballBeat, color: .red, padding: nil)
        view.addSubview(loader)
        loader.startAnimating()
    }

    private func setup() {
        tableView.delegate = self
        tableView.dataSource = self

        tableView.tableFooterView = UIView()

        let nib = UINib(nibName: Constants.itemCellIdentifier, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: Constants.itemCellIdentifier)
    }

    private func showDetails(with item: Item) {
        let detailsViewController = ListDetailViewController(item: item)
        navigationController?.pushViewController(detailsViewController, animated: true)
    }
}

extension ListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.itemCellIdentifier, for: indexPath) as? ItemCell
            else { return UITableViewCell() }
        let currentItem = items[indexPath.row]
        cell.render(item: currentItem)
        return cell

    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let currentItem = items[indexPath.row]
        showDetails(with: currentItem)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 190
    }
}
