//
//  ViewController.swift
//  G67L13
//
//  Created by Ivan Vasilevich on 12/20/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var emailTextField: UITextField!
	
	let kEmail = "iyf2h843hf879437vg34"
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		var nsArray = [1, 2, 3] as NSArray
		nsArray = [1, 2]
		nsArray = nsArray.mutableCopy() as! NSArray
		let nsMutArr = nsArray.mutableCopy() as! NSMutableArray
		nsMutArr.add(9)
		print(nsMutArr)
		nsMutArr.write(toFile: "/Users/ivanvasilevich/Desktop/пряники.STL", atomically: false)
		fuserDefaultsExample()
		
//		view.backgroundColor = UIColor.init(red: 	255	/ 255.0,
//											green: 	242 / 255.0,
//											blue: 	0	/ 255.0,
//											alpha: 	100 / 100.0)
		view.backgroundColor = UIColor.init(hex: 0xFFF200)
	}
	
	func fuserDefaultsExample() {
		let a = UserDefaults.standard.string(forKey: kEmail)
		emailTextField.text = a
		emailTextField.font = emailTextField.font!.withSize(32)
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		UserDefaults.standard.set(emailTextField.text, forKey: kEmail)
		showMessageAlert("YAZZZ")
	}
	
	func showMessageAlert(_ message: String = "") {
		let alert = UIAlertController(title: "Ahtung!!!", message: message, preferredStyle: .actionSheet)
		
		let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
			print(action.title!)
		}
		alert.addAction(okAction)
		
		let cancelAction = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
		alert.addAction(cancelAction)
		
		if UIDevice.current.userInterfaceIdiom == .pad {
			alert.popoverPresentationController?.sourceView = emailTextField
		}
		
		present(alert, animated: true, completion: nil)
	}

}

