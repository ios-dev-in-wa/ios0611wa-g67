//
//  MasterViewController.swift
//  G67L12
//
//  Created by Ivan Vasilevich on 12/18/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit
import Parse

class MasterViewController: UITableViewController {

	var detailViewController: DetailViewController? = nil
	var objects = [PFObject]()


	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.

		let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(insertNewObject(_:)))
		navigationItem.rightBarButtonItem = addButton
		if let split = splitViewController {
		    let controllers = split.viewControllers
		    detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
		}
		getDrinks()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		if PFUser.current() == nil {
			let loginVC = PFLogInViewController()
			loginVC.delegate = self
			present(loginVC, animated: true, completion: nil)
		}
	}

	@objc
	func insertNewObject(_ sender: Any) {
		let newDrink = PFObject(className: "Drink")
		newDrink["sugar"] = 2
		newDrink["milk"] = 1
		newDrink["water"] = 500
		newDrink["name"] = "Water"
		newDrink.saveInBackground { (success, error) in
			if success {
				print("drink added to db")
			}
			else {
				print(error!.localizedDescription)
			}
		}
		
		objects.insert(newDrink, at: 0)
		let indexPath = IndexPath(row: 0, section: 0)
		tableView.insertRows(at: [indexPath], with: .automatic)
	}

	private func getDrinks() {
		let query = PFQuery(className: "Drink")
//		query.limit = 10000
//		query.where...
		query.findObjectsInBackground { (objects, error) in
			let objectsToDisplay = objects ?? [PFObject]()
			
			for obj in objectsToDisplay  {
				print("\n-------------------------")
				print(obj["name"] as! String)
			}
			
			self.objects = objectsToDisplay
			self.tableView.reloadData()
		}
	}
	
	// MARK: - Segues

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "showDetail" {
		    if let indexPath = tableView.indexPathForSelectedRow {
		        let object = objects[indexPath.row]
		        let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
		        controller.detailItem = object
		        controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
		        controller.navigationItem.leftItemsSupplementBackButton = true
		    }
		}
	}

	// MARK: - Table View

	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return objects.count
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

		let object = objects[indexPath.row]
		cell.textLabel!.text = object["name"] as? String ?? "NO NAME"
		return cell
	}

}

extension MasterViewController: PFLogInViewControllerDelegate {
	func log(_ logInController: PFLogInViewController, didLogIn user: PFUser) {
		logInController.dismiss(animated: true, completion: nil)
	}
}
