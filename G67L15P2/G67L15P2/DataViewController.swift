//
//  DataViewController.swift
//  G67L15P2
//
//  Created by Ivan Vasilevich on 1/8/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class DataViewController: UIViewController {
	
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var dataLabel: UILabel!
	var dataObject: String = "" {
		didSet {
			print(dataObject)
		}
	}


	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		imageView.image = UIImage(named: dataObject)
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.dataLabel!.text = dataObject
	}


}

