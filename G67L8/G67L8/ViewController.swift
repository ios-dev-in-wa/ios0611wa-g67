//
//  ViewController.swift
//  G67L8
//
//  Created by Ivan Vasilevich on 11/29/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var blueView: UIView!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		//-------------------------------------------------
		if let redView = blueView.superview {
			if let greenView = redView.superview {
				greenView.backgroundColor = .brown
			}
		}
		//-------------------------------------------------
		guard let redView = blueView.superview else {
			return
		}
		
		guard let greenView = redView.superview else {
			return
		}
		
		greenView.backgroundColor = .brown
		//-------------------------------------------------
		blueView.superview?.superview?.backgroundColor = .brown //
	}

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let secondVC = segue.destination as? SecondVC {
			secondVC.message = "YAZZZ!"
			secondVC.prevVC = self
		}
	}
	
	@IBAction func buttonPressed() {
		performSegue(withIdentifier: "showGrapes", sender: nil)
	}
	
	@IBAction func showVCbyStoryboardId() {
		let storyboard = self.storyboard!
		let grapesVC = storyboard.instantiateViewController(withIdentifier: "grapes") as! SecondVC
		grapesVC.message = "MMMMMMMAMAMa"
		navigationController?.pushViewController(grapesVC, animated: true)
	}
	
	override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
		return false
	}
}

