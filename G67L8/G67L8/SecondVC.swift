//
//  SecondVC.swift
//  G67L8
//
//  Created by Ivan Vasilevich on 11/29/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

func log(_ functionName: String = #function, line: Int = #line, file: String = #file, message: String = "") {
	let dateFormatter = DateFormatter()
	dateFormatter.dateFormat = "HH:mm:ss"
	print("\(dateFormatter.string(from: Date())) l#:\(line) \(functionName) in \((file as NSString).lastPathComponent) " + message)
}

class SecondVC: UIViewController {
	
	weak var prevVC: ViewController!
	var message = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
		let kakayetoXpen = """
'
"""
		view.backgroundColor = .red
		
		let a = kakayetoXpen + message + kakayetoXpen
		print(a)
		navigationItem.title = message
    }
	
	@IBAction func doneBtnPressed() {
		log()
		navigationController?.popViewController(animated: true)
		prevVC.blueView.isHidden = true
	}
	
}
