//
//  FiltersTVC.swift
//  G67L10
//
//  Created by Ivan Vasilevich on 12/11/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class FiltersTVC: UITableViewController {

	@IBOutlet weak var maxTextField: UITextField!
	@IBOutlet weak var minTextField: UITextField!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		maxTextField.delegate = self
	}

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return super.numberOfSections(in: tableView)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return super.tableView(tableView, numberOfRowsInSection: section)
    }

}

extension FiltersTVC: UITextFieldDelegate {
	func textFieldDidEndEditing(_ textField: UITextField) {
		guard textField == maxTextField else {
			return
		}
		let minValue = Int(minTextField.text!) ?? 0
		let maxValue = Int(maxTextField.text!) ?? 0
		minTextField.text = min(minValue, maxValue).description
		maxTextField.text = max(minValue, maxValue).description
	}
}
