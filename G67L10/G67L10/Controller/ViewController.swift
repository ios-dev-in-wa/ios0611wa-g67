//
//  ViewController.swift
//  G67L10
//
//  Created by Ivan Vasilevich on 12/11/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		view.backgroundColor = .red
		
		let morda = FaceView(frame: CGRect(x: 100, y: 250, width: 256, height: 256))
		morda.backgroundColor = .green
		morda.faceColor = .green
		morda.changeHappyFace(withHappLevel: 35, andColor: .yellow)
		view.addSubview(morda)
	}


}

