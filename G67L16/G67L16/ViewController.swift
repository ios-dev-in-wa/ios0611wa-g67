//
//  ViewController.swift
//  G67L16
//
//  Created by Ivan Vasilevich on 1/11/19.
//  Copyright © 2019 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	var topLabel: UILabel? {
		return view.viewWithTag(555) as? UILabel
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		
		topLabel?.text = NSLocalizedString("Created by Ivan Vasilevich", comment: "YAZZ!!!")
	}


}

