//
//  ViewController.swift
//  G67L14
//
//  Created by Ivan Vasilevich on 12/27/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		sendDrinksRequest()
	}
	
	func sendDrinksRequest() {
		/**
		Drinks
		get https://parseapi.back4app.com/classes/Drink
		*/
		
		// Add Headers
		let headers = [
			"X-Parse-REST-API-Key":"SPOlnhn8PZYSoPgito3CQP2oUtyVRCGjMCxgckkJ",
			"X-Parse-Application-Id":"yu2d52irD2DSZtCdGqLQj1HliRFsmGLFM5ybxzSf",
			]
		
		// Fetch Request
		Alamofire.request("https://parseapi.back4app.com/classes/Drink", method: .get, headers: headers).responseArray(queue: DispatchQueue.main, keyPath: "results", context: nil) { (response: DataResponse<[Drink]>) in
			
			let forecastArray = response.result.value
			
			if let drinksArray = forecastArray {
				for drink in drinksArray {
					print(drink.name!)
					print(drink.objectId!)
				}
			}
		}
		
		
	}
	
}

