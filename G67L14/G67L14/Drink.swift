//
//  Drink.swift
//  G67L14
//
//  Created by Ivan Vasilevich on 12/27/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit
import ObjectMapper

class Drink: Mappable {
	
	var objectId: String?
	var water: String?
	var name: String?
	
	func mapping(map: Map) {
		objectId <- map["objectId"]
		water <- map["water"]
		name <- map["name"]
	}
	
	required init?(map: Map) {
		
	}

}
