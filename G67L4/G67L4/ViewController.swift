//
//  ViewController.swift
//  G67L4
//
//  Created by Ivan Vasilevich on 11/15/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
//		workWithStrings()
		workWithArrays()
	}
	
	func workWithStrings() {
		let name: String = "Ivan"
		let greeting = "Hello, my name\nis \"\(name)\""
		print(greeting)
		let commentsStr = """
//
//  ViewController.swift
//  G67L4
//
		//  Created by Ivan Vasilevich on 11/15/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//
"""
		print(commentsStr)
		
		if greeting.hasPrefix("hell") {
			print("Highway to hell")
		}
		else {
			print("kittens")
		}
	}

	func workWithArrays() {
		var numbers = [1, 5, 9, 3, 1, 5, 9, 3, 4, 4, 90, 1, 101]
		print("today lucky numbers is: \(numbers)")
		let luckyNum3 = numbers[numbers.count - 1]
//		let luckyNum3 = numbers.last
		print("lucky number #3 =", luckyNum3)
		print("today lucky numbers is: \(numbers)")
		numbers.remove(at: 4)
		print("today lucky numbers is: \(numbers)")
		
		var ticTacToeArr = Array.init(repeating: "X", count: 9)
		drawGameField(array: ticTacToeArr)
		
		let diagonalNumbers = [0, 4, 8]
		diagonalNumbers.forEach { (element) in
			ticTacToeArr[element] = "O"
		}
//		ticTacToeArr[4] = "O"
//		ticTacToeArr[1] = "O"
//		ticTacToeArr[9] = "O"
		drawGameField(array: ticTacToeArr)
		
		print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n", numbers)
//		print("\n", numbers)
		let indexOf4InNumbersArray = numbers.firstIndex(of: 4)
		
		print("indexOf4InNumbersArray", indexOf4InNumbersArray!)
		
		
		let numberString = "245.0"
		let numberFromString = Int(numberString)
		print(numberFromString! + 5)
		
	}
	
	func drawGameField(array: [String]) {
		var gameField = ""
		for i in 0..<array.count {
			let element = array[i]
			gameField = gameField + "\t\(i) : " + element
			if (i + 1) % 3 == 0 {
				gameField += "\n"
			}
		}
		print(gameField)
	}

}

