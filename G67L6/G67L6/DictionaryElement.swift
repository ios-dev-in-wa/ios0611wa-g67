//
//  DictionaryElement.swift
//  G67L6
//
//  Created by Ivan Vasilevich on 11/22/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

struct Drink {
	var water: Int = 5
	var sugar: Int
//	var mint: Int = 0
}

class DictionaryElement {
	
	var word: String
	let definition: String
	private let examples: [String]
	
	 var description: String {
		Drink.init(water: 1, sugar: 2)
		var result = word + " = " + definition + "\n"
		for exampleString in examples {
			result += exampleString
			if examples.last != exampleString {
				result += "\n"
			}
		}
		return result
	}
	
	init(word: String, definition: String, examples: [String]) {
		self.word = word
		self.definition = definition
		self.examples = examples
	}
	
	func sayTheWord() {
		print(word)
	}
	
}

class Rect {
	var width: Int = 0
	var height: Int = 0
	var perimeter: Int {
		let result = width * 2 + height * 2
		return result
	}
}
