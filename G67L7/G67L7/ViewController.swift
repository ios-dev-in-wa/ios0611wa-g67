//
//  ViewController.swift
//  G67L7
//
//  Created by Ivan Vasilevich on 11/27/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	@IBOutlet weak var centerLabel: UILabel!
	var counter: Int = 0
	
	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
//		Int.random(in: 2...5)
		let el1 = DictionaryElement.init(word: "123456789", definition: "1234567890", examples: [])
		let el2 = el1
		print(el1.word)
		changeElement(el2)
		print(el1.word)
		print(el2.word)
		buttonPressed()
		centerLabel.text = el1.word
	}
	
	func changeElement(_ element: DictionaryElement) {
		var copyOfEl = element
		copyOfEl.word = "qwertyuiop"
		centerLabel.text = String(Int.random(in: 0...9999999))
	}
	
	@IBAction func buttonPressed() {
		centerLabel.backgroundColor = UIColor(red: CGFloat(arc4random_uniform(256))/255,
											  green: CGFloat(arc4random_uniform(256))/255,
											  blue: CGFloat(arc4random_uniform(256))/255,
											  alpha: 1)
	}

	@IBAction func blackNWhiteButtonPressed(_ sender: UIButton) {
		let pantToRed = sender.tag == 1
		pantToRed ? print("red") : print("white")
		view.backgroundColor = pantToRed ? .red : .white
		
//		sender.isEnabled = false
	}
	
}

