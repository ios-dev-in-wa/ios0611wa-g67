//
//  ViewController.swift
//  G67L11
//
//  Created by Ivan Vasilevich on 12/13/18.
//  Copyright © 2018 RockSoft. All rights reserved.
// http://fuckingclosuresyntax.com/

import UIKit

class ViewController: UIViewController {
	
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var nameLabel: UILabel!
	
	var objects = ["Startup", "SiliconValley", "Rick and Morty"]
	
	var currentObject = -1

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		let sortedArray = [100, 3, 4, 9].sorted { (a, b) -> Bool in
			return a < b
		}
		print(sortedArray)
		displayNextObject()
	}
	
	func displayNextObject() {
		currentObject += 1
		
		let name = objects[currentObject]
		let image = UIImage(named: name)
		
		UIView.transition(with: view, duration: 0.5, options: [.transitionFlipFromRight], animations: {
			self.nameLabel.text = name
			self.imageView.image = image
		}, completion: nil)
		
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		displayNextObject()
	}
	
	@objc func foo() {
		
	}

	@IBAction func panRecognized(_ sender: UIPanGestureRecognizer) {
		sender.view?.center = sender.location(in: view)
	}
	
	@IBAction func imagePressed(_ sender: UITapGestureRecognizer) {
		print("imagePressed")
		
		
		let blockLink: () -> Void = {
			sender.view?.frame.size.width /= 2
		}
		UIView.animate(withDuration: 2,
					   animations: blockLink,
					   completion: nil)
		
		UIView.animate(withDuration: 3,
					   animations: {
			sender.view?.frame.size.width /= 2
		}) { (finished) in
			if finished {
				sender.view?.isHidden = true
			}
		}
		
	}
}

