//
//  ViewController.swift
//  G67L3
//
//  Created by Ivan Vasilevich on 11/13/18.
//  Copyright © 2018 RockSoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		for x in 0..<5 {
			for y in 0..<3 {
				print("cell coordinates = (\(x), \(y))")
			}
		}
	}
	
	func anotherLoop() {
		var x = 0
		while x < 5 {
			for y in 0..<3 {
				print("cell coordinates = (\(x), \(y))")
			}
			x = x + 4
		}
	}


}

